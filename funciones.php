<?php 
	require_once 'vendor\autoload.php';

	use Illuminate\Database\Capsule\Manager as Capsule;
	use Illuminate\Database\Eloquent\Model;

	$capsule = new Capsule();

	$capsule->addConnection([
		'driver'    => 'mysql',
		'host'      => 'localhost',
		'database'  => 'infotab',
		'username'  => 'root',
		'password'  => '',
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix'    => '',
	]);

	// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
	$capsule->bootEloquent();

	class Presupuesto extends Model {
		protected $table = "presupuesto";
		public function detalles() {
			return $this->hasMany('Detalle','id_presupuesto','id');
		}
	}

	class Detalle extends Model {
		protected $table = "items_trans";

	}

?>